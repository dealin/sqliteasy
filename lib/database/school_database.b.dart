// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: DatabaseBuilder
// **************************************************************************

import "package:sqliteasy_core/sqliteasy_core.dart";
import "package:easy_sqlite/database/school_database.dart";

class SchoolDatabaseImpl$_$ extends SchoolDatabase {
  @override
  int get version => 8;
  @override
  String get identityHash => "7c4daf075a04d3629d7cd298b1f6bdb8";

  @override
  void onCreate() {
    exec(
        "CREATE TABLE IF NOT EXISTS `student` (`id` INTEGER,`name` TEXT,`class_id` INTEGER,`is_male` INTEGER,`birth_day` INTEGER,PRIMARY KEY (id),FOREIGN KEY(`class_id`) REFERENCES `class`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
    exec(
        "CREATE TABLE IF NOT EXISTS `class` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,`name` INTEGER,`class_teacher_id` INTEGER,FOREIGN KEY(`class_teacher_id`) REFERENCES `teacher`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
    exec(
        "CREATE TABLE IF NOT EXISTS `teacher` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,`is_male` INTEGER,`name` TEXT,`age` INTEGER,`height` INTEGER,`weight` REAL)");
    exec(
        "CREATE TABLE IF NOT EXISTS `course` (`id` INTEGER PRIMARY KEY AUTOINCREMENT)");
    exec(
        "CREATE UNIQUE INDEX IF NOT EXISTS `indexName` ON `student` (`id`,`name`,`class_id`)");

    exec(
        "CREATE TABLE IF NOT EXISTS sqliteasy_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    exec(
        "INSERT OR REPLACE INTO sqliteasy_master_table (id, identity_hash) VALUES(1, '${identityHash}')");
  }

  @override
  Future<SchemaValidateResult> validateSchema(SupportSQLiteDatabase db) async {
    final studentColumns = Set<ColumnInfo>();
    studentColumns.add(ColumnInfo(
      name: "id",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 1,
    ));
    studentColumns.add(ColumnInfo(
      name: "name",
      type: SQLiteTypes.getTypeFromName("TEXT"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    studentColumns.add(ColumnInfo(
      name: "class_id",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    studentColumns.add(ColumnInfo(
      name: "is_male",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    studentColumns.add(ColumnInfo(
      name: "birth_day",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));

    final studentForeignKeys = Set<ForeignKeyInfo>();
    studentForeignKeys.add(ForeignKeyInfo(
      referenceTable: "class",
      onUpdate: Action.NO_ACTION,
      onDelete: Action.NO_ACTION,
      columns: ["class_id"],
      referenceColumns: ["id"],
    ));

    final studentIndexes = Set<IndexInfo>();
    studentIndexes.add(IndexInfo(
      name: "indexName",
      unique: true,
      columns: ["id", "name", "class_id"],
    ));

    final studentTableInfo = TableInfo(
      name: "student",
      columns: studentColumns,
      foreignKeys: studentForeignKeys,
      indexes: studentIndexes,
    );
    final studentExistedInfo = await TableInfo.read(db, "student");
    if (studentExistedInfo != null && studentExistedInfo != studentTableInfo) {
      return SchemaValidateResult(
          isValid: false,
          errorMsg:
              "student(package:easy_sqlite/database/school_entities.dart).\nExpected:\n${studentTableInfo}\nFound:\n${studentExistedInfo}");
    }

    final classColumns = Set<ColumnInfo>();
    classColumns.add(ColumnInfo(
      name: "id",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 1,
    ));
    classColumns.add(ColumnInfo(
      name: "name",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    classColumns.add(ColumnInfo(
      name: "class_teacher_id",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));

    final classForeignKeys = Set<ForeignKeyInfo>();
    classForeignKeys.add(ForeignKeyInfo(
      referenceTable: "teacher",
      onUpdate: Action.NO_ACTION,
      onDelete: Action.NO_ACTION,
      columns: ["class_teacher_id"],
      referenceColumns: ["id"],
    ));

    final classIndexes = Set<IndexInfo>();

    final classTableInfo = TableInfo(
      name: "class",
      columns: classColumns,
      foreignKeys: classForeignKeys,
      indexes: classIndexes,
    );
    final classExistedInfo = await TableInfo.read(db, "class");
    if (classExistedInfo != null && classExistedInfo != classTableInfo) {
      return SchemaValidateResult(
          isValid: false,
          errorMsg:
              "class(package:easy_sqlite/database/school_entities.dart).\nExpected:\n${classTableInfo}\nFound:\n${classExistedInfo}");
    }

    final teacherColumns = Set<ColumnInfo>();
    teacherColumns.add(ColumnInfo(
      name: "id",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 1,
    ));
    teacherColumns.add(ColumnInfo(
      name: "is_male",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    teacherColumns.add(ColumnInfo(
      name: "name",
      type: SQLiteTypes.getTypeFromName("TEXT"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    teacherColumns.add(ColumnInfo(
      name: "age",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    teacherColumns.add(ColumnInfo(
      name: "height",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));
    teacherColumns.add(ColumnInfo(
      name: "weight",
      type: SQLiteTypes.getTypeFromName("REAL"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 0,
    ));

    final teacherForeignKeys = Set<ForeignKeyInfo>();

    final teacherIndexes = Set<IndexInfo>();

    final teacherTableInfo = TableInfo(
      name: "teacher",
      columns: teacherColumns,
      foreignKeys: teacherForeignKeys,
      indexes: teacherIndexes,
    );
    final teacherExistedInfo = await TableInfo.read(db, "teacher");
    if (teacherExistedInfo != null && teacherExistedInfo != teacherTableInfo) {
      return SchemaValidateResult(
          isValid: false,
          errorMsg:
              "teacher(package:easy_sqlite/database/school_entities.dart).\nExpected:\n${teacherTableInfo}\nFound:\n${teacherExistedInfo}");
    }

    final courseColumns = Set<ColumnInfo>();
    courseColumns.add(ColumnInfo(
      name: "id",
      type: SQLiteTypes.getTypeFromName("INTEGER"),
      notNull: false,
      defaultValue: null,
      primaryKeyPosition: 1,
    ));

    final courseForeignKeys = Set<ForeignKeyInfo>();

    final courseIndexes = Set<IndexInfo>();

    final courseTableInfo = TableInfo(
      name: "course",
      columns: courseColumns,
      foreignKeys: courseForeignKeys,
      indexes: courseIndexes,
    );
    final courseExistedInfo = await TableInfo.read(db, "course");
    if (courseExistedInfo != null && courseExistedInfo != courseTableInfo) {
      return SchemaValidateResult(
          isValid: false,
          errorMsg:
              "course(package:easy_sqlite/database/school_entities.dart).\nExpected:\n${courseTableInfo}\nFound:\n${courseExistedInfo}");
    }

    return SchemaValidateResult(isValid: true);
  }
}

/// ==========DaoImpl==========

/// ==========ValueConverters==========
