import 'dart:typed_data';

import 'package:sqliteasy_core/annotations/database.dart';

@Entity()
class EntityTest {
  @PrimaryKey()
  @Column(primaryKeyPosition: 1)
  Uint8List id;

  @PrimaryKey()
  @Column(primaryKeyPosition: 0)
  int id2;
}