
import 'package:sqliteasy_core/annotations/database.dart';

@Entity(foreignKeys: [
    ForeignKey(entity: Class, columns: ["class_id"], referenceColumns: ["id"])
],
indices: [
  Index(["id", "name", "class_id"], unique: true, name: "indexName")
])
class Student {
  @PrimaryKey()
  int id;
  String name;
  int classId;
  bool isMale;
  DateTime birthDay;
}

@Entity(
  foreignKeys: [ForeignKey(entity: Teacher, columns: ["class_teacher_id"], referenceColumns: ["id"])]
)
class Class {
  @PrimaryKey(autoGenerate: true)
  int id;
  int name;
  int classTeacherId;
}

@Entity()
class Teacher {
  @PrimaryKey(autoGenerate: true)
  int id;
  bool isMale;
  String name;
  int age;
  int height;
  double weight;
}

@Entity()
class Course {
  @PrimaryKey(autoGenerate: true)
  int id;
}