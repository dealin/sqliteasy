import 'package:easy_sqlite/database/school_entities.dart';
import 'package:sqliteasy_core/sqliteasy_core.dart';

@Database([Student, Class, Teacher, Course], 8)
abstract class SchoolDatabase extends SQLiteasyDatabase{

}