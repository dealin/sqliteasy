import 'dart:typed_data';

import 'package:sqliteasy_core/sqliteasy_core.dart';

@Database([LocalMoment], 1)
abstract class MomentDatabase extends SQLiteasyDatabase {
  MomentDao momentDao();
}

@Entity(tableName: "local_moment")
class LocalMoment {
  @PrimaryKey()
  int? id;
  String? content;
  String? assets;
  /// 错误码
  int? code;
  int? time;
}

@Dao()
abstract class MomentDao {
  Future<bool> insert(LocalMoment moment) async => (await insertBatch([moment])) == 1;
  @Insert()
  Future<int> insertBatch(List<LocalMoment> list);

  Future<bool> delete(LocalMoment moment) async => (await deleteBatch([moment])) == 1;
  @Delete()
  Future<int> deleteBatch(List<LocalMoment> list);

  @Query("select * from local_moment")
  Future<List<LocalMoment>> listAll();
}