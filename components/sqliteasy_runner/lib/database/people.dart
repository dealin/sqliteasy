import 'dart:typed_data';

import 'package:sqliteasy_core/annotations/database.dart';

@Entity(primaryKeys: ["age"])
class People {
  String name;
  int age;
  Uint8List? bytes;
  double? fraction;
  @Column()
  int? humpTest;

  People(this.age, this.name);
}
