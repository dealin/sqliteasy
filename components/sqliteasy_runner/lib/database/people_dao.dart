import 'package:sqliteasy_runner/database/people.dart';
import 'package:sqliteasy_core/annotations/dao.dart';
import 'package:sqliteasy_core/enums/conflict_strategy.dart';
import 'package:sqliteasy_core/core/sqliteasy_database.dart';

@Dao()
abstract class PeopleDao {

  @Insert(conflictStrategy: ConflictStrategy.replace)
  Future<int> insert({List<People> people});

  @Insert()
  Future<int> insertOne(People people);

  @Delete()
  Future<int> delete(List<People> people);

  @Delete()
  Future<int> deleteOne(People people);

  @Delete(entity: People)
  Future<int> deleteByKey(int age1);

  @Update()
  Future<int> updateBatch(List<People> people);

  @Update()
  void update(People people);

  @Update()
  Future<void> updateWithReturnValue(People people);

  @Query("select * from people where age = :age1")
  Future<People> query(int age1);

  @Query("select * from people where age = :age1")
  void queryWithoutReturnValue(int age1);

  @Query("select * from people where age = :age1")
  Future<List<People>> queryWithoutReturnValue1(int age1);

  @Query("select * from people where age = :age1")
  Future<Set<People>> queryWithoutReturnValue2(int age1);

  @Query("select * from people")
  Future<List<People>> listAll();

}