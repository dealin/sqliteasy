class SQLiteasyBuildError extends Error {
  final String error;
  final String location;
  SQLiteasyBuildError(this.error, [this.location]): super();

  @override
  String toString() {
    return "${this.runtimeType}: ${error}${this.location?.isNotEmpty == true ? "\nat ${this.location}" : ""}\n${this.stackTrace}";
  }
}