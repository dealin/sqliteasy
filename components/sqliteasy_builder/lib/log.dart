void log(tag, content) {
  print("${DateTime.now()} ${tag}: ${content}");
}