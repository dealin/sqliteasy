class Const {
  static final dbFieldName = "_db";

  static final packageAnnotation = "package:sqliteasy_core/sqliteasy_core.dart";
  static final packageCore = "package:sqliteasy_core/sqliteasy_core.dart";
}