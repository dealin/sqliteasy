
import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:sqliteasy_core/sqliteasy_core.dart';

class SQLiteasyUtil {
  static String getElementDesc(DartType element) {
    // 注意处理async*
    return element?.toString()?.replaceAll("*", "") ?? "void";
  }


  static String humpToUnderline(String name) {
    if (name.isEmpty) {
      return "";
    }
    StringBuffer sb = StringBuffer(name[0].toLowerCase());
    for (int i = 1; i < name.length; i++) {
      var char = name[i];
      if (char.toLowerCase() != char && i > 0) {
        sb.write("_${char.toLowerCase()}");
      } else {
        sb.write(char);
      }
    }
    return sb.toString();
  }

  static String getNullValueStrategyStr(NullValueStrategy nullValueStrategy) {
    return nullValueStrategy.toString();
  }
}