import 'package:antlr4/antlr4.dart';
import 'package:antlr4/src/recognizer.dart';
import 'package:sqliteasy_builder/errors.dart';
import 'package:sqliteasy_builder/sql_checker/antrl/SQLiteLexer.dart';
import 'package:sqliteasy_builder/sql_checker/antrl/SQLiteParserVisitor.dart';
import 'package:sqliteasy_builder/sql_checker/visitor.dart';

import 'antrl/SQLiteParser.dart';

class TreeShapeListener implements ParseTreeListener {
  @override
  void enterEveryRule(ParserRuleContext ctx) {
    print(ctx.text);
  }

  @override
  void exitEveryRule(ParserRuleContext node) {
  }

  @override
  void visitErrorNode(ErrorNode node) {
    print("error:${node}");
  }

  @override
  void visitTerminal(TerminalNode node) {
  }
}

void main(List<String> args) async {
  SQLiteLexer.checkVersion();
  SQLiteParser.checkVersion();
  final input = await InputStream.fromPath("G:\\Flutter\\sqliteasy\\components\\sqliteasy_builder\\lib\\sql_checker\\example\\test.sql");
  final lexer = SQLiteLexer(input);
  final tokens = CommonTokenStream(lexer);
  final parser = SQLiteParser(tokens);
  parser.addErrorListener(SQLiteCheckerErrorListener());
  parser.buildParseTree = true;
  final tree = parser.parse();
  SQLiteCheckerVisitor().visit(tree);
}

class SQLiteChecker {
  void check(String source) {
    final lexer = SQLiteLexer(InputStream.fromString(source));
    final tokens = CommonTokenStream(lexer);
    final parser = SQLiteParser(tokens);
    parser.addErrorListener(SQLiteCheckerErrorListener());
    parser.buildParseTree = true;
    final tree = parser.parse();
    SQLiteCheckerVisitor().visit(tree);
  }
}
class SQLiteCheckerErrorListener extends BaseErrorListener {

  @override
  void syntaxError(Recognizer<ATNSimulator> recognizer, Object offendingSymbol, int line, int charPositionInLine,
      String msg, RecognitionException e) {
    throw SQLiteasyBuildError("sqlite syntax error at line:${line}:${charPositionInLine} ${msg}");
  }
}