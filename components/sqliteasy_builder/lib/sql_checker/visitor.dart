import 'package:sqliteasy_builder/sql_checker/antrl/SQLiteParser.dart';

import 'antrl/SQLiteParserVisitor.dart';

class SQLiteCheckerVisitor extends SQLiteParserVisitor {
  @override
  visitAggregate_func(Aggregate_funcContext ctx) {
    // TODO: implement visitAggregate_func
    
  }

  @override
  visitAggregate_function_invocation(Aggregate_function_invocationContext ctx) {
    // TODO: implement visitAggregate_function_invocation
    
  }

  @override
  visitAlias(AliasContext ctx) {
    // TODO: implement visitAlias
    
  }

  @override
  visitAlter_table_stmt(Alter_table_stmtContext ctx) {
    // TODO: implement visitAlter_table_stmt
    
  }

  @override
  visitAnalyze_stmt(Analyze_stmtContext ctx) {
    // TODO: implement visitAnalyze_stmt
    
  }

  @override
  visitAny_name(Any_nameContext ctx) {
    // TODO: implement visitAny_name
    
  }

  @override
  visitAsc_desc(Asc_descContext ctx) {
    // TODO: implement visitAsc_desc
    
  }

  @override
  visitAttach_stmt(Attach_stmtContext ctx) {
    // TODO: implement visitAttach_stmt
    
  }

  @override
  visitBase_window_name(Base_window_nameContext ctx) {
    // TODO: implement visitBase_window_name
    
  }

  @override
  visitBegin_stmt(Begin_stmtContext ctx) {
    // TODO: implement visitBegin_stmt
    
  }

  @override
  visitCollation_name(Collation_nameContext ctx) {
    // TODO: implement visitCollation_name
    
  }

  @override
  visitColumn_alias(Column_aliasContext ctx) {
    // TODO: implement visitColumn_alias
    
  }

  @override
  visitColumn_constraint(Column_constraintContext ctx) {
    // TODO: implement visitColumn_constraint
    
  }

  @override
  visitColumn_def(Column_defContext ctx) {
    // TODO: implement visitColumn_def
    
  }

  @override
  visitColumn_name(Column_nameContext ctx) {
    // TODO: implement visitColumn_name
    
  }

  @override
  visitColumn_name_list(Column_name_listContext ctx) {
    // TODO: implement visitColumn_name_list
    
  }

  @override
  visitCommit_stmt(Commit_stmtContext ctx) {
    // TODO: implement visitCommit_stmt
    
  }

  @override
  visitCommon_table_expression(Common_table_expressionContext ctx) {
    // TODO: implement visitCommon_table_expression
    
  }

  @override
  visitCommon_table_stmt(Common_table_stmtContext ctx) {
    // TODO: implement visitCommon_table_stmt
    
  }

  @override
  visitCompound_operator(Compound_operatorContext ctx) {
    // TODO: implement visitCompound_operator
    
  }

  @override
  visitCompound_select_stmt(Compound_select_stmtContext ctx) {
    // TODO: implement visitCompound_select_stmt
    
  }

  @override
  visitConflict_clause(Conflict_clauseContext ctx) {
    // TODO: implement visitConflict_clause
    
  }

  @override
  visitCreate_index_stmt(Create_index_stmtContext ctx) {
    // TODO: implement visitCreate_index_stmt
    
  }

  @override
  visitCreate_table_stmt(Create_table_stmtContext ctx) {
    // TODO: implement visitCreate_table_stmt
    
  }

  @override
  visitCreate_trigger_stmt(Create_trigger_stmtContext ctx) {
    // TODO: implement visitCreate_trigger_stmt
    
  }

  @override
  visitCreate_view_stmt(Create_view_stmtContext ctx) {
    // TODO: implement visitCreate_view_stmt
    
  }

  @override
  visitCreate_virtual_table_stmt(Create_virtual_table_stmtContext ctx) {
    // TODO: implement visitCreate_virtual_table_stmt
    
  }

  @override
  visitCte_table_name(Cte_table_nameContext ctx) {
    // TODO: implement visitCte_table_name
    
  }

  @override
  visitDefault_DEFAULT__value(Default_DEFAULT__valueContext ctx) {
    // TODO: implement visitDefault_DEFAULT__value
    
  }

  @override
  visitDelete_stmt(Delete_stmtContext ctx) {
    // TODO: implement visitDelete_stmt
    
  }

  @override
  visitDelete_stmt_limited(Delete_stmt_limitedContext ctx) {
    // TODO: implement visitDelete_stmt_limited
    
  }

  @override
  visitDetach_stmt(Detach_stmtContext ctx) {
    // TODO: implement visitDetach_stmt
    
  }

  @override
  visitDrop_stmt(Drop_stmtContext ctx) {
    // TODO: implement visitDrop_stmt
    
  }

  @override
  visitError_message(Error_messageContext ctx) {
    // TODO: implement visitError_message
    
  }

  @override
  visitExpr(ExprContext ctx) {
    // TODO: implement visitExpr
    
  }

  @override
  visitExpr_asc_desc(Expr_asc_descContext ctx) {
    // TODO: implement visitExpr_asc_desc
    
  }

  @override
  visitFactored_select_stmt(Factored_select_stmtContext ctx) {
    // TODO: implement visitFactored_select_stmt
    
  }

  @override
  visitFilename(FilenameContext ctx) {
    // TODO: implement visitFilename
    
  }

  @override
  visitFilter_clause(Filter_clauseContext ctx) {
    // TODO: implement visitFilter_clause
    
  }

  @override
  visitForeign_key_clause(Foreign_key_clauseContext ctx) {
    // TODO: implement visitForeign_key_clause
    
  }

  @override
  visitForeign_table(Foreign_tableContext ctx) {
    // TODO: implement visitForeign_table
    
  }

  @override
  visitFrame_clause(Frame_clauseContext ctx) {
    // TODO: implement visitFrame_clause
    
  }

  @override
  visitFrame_left(Frame_leftContext ctx) {
    // TODO: implement visitFrame_left
    
  }

  @override
  visitFrame_right(Frame_rightContext ctx) {
    // TODO: implement visitFrame_right
    
  }

  @override
  visitFrame_single(Frame_singleContext ctx) {
    // TODO: implement visitFrame_single
    
  }

  @override
  visitFrame_spec(Frame_specContext ctx) {
    // TODO: implement visitFrame_spec
    
  }

  @override
  visitFunction_name(Function_nameContext ctx) {
    // TODO: implement visitFunction_name
    
  }

  @override
  visitIndex_name(Index_nameContext ctx) {
    // TODO: implement visitIndex_name
    
  }

  @override
  visitIndexed_column(Indexed_columnContext ctx) {
    // TODO: implement visitIndexed_column
    
  }

  @override
  visitInitial_select(Initial_selectContext ctx) {
    // TODO: implement visitInitial_select
    
  }

  @override
  visitInsert_stmt(Insert_stmtContext ctx) {
    // TODO: implement visitInsert_stmt
    
  }

  @override
  visitJoin_clause(Join_clauseContext ctx) {
    // TODO: implement visitJoin_clause
    
  }

  @override
  visitJoin_constraint(Join_constraintContext ctx) {
    // TODO: implement visitJoin_constraint
    
  }

  @override
  visitJoin_operator(Join_operatorContext ctx) {
    // TODO: implement visitJoin_operator
    
  }

  @override
  visitKeyword(KeywordContext ctx) {
    // TODO: implement visitKeyword
    
  }

  @override
  visitLimit_stmt(Limit_stmtContext ctx) {
    // TODO: implement visitLimit_stmt
    
  }

  @override
  visitLiteral_value(Literal_valueContext ctx) {
    // TODO: implement visitLiteral_value
    
  }

  @override
  visitModule_argument(Module_argumentContext ctx) {
    // TODO: implement visitModule_argument
    
  }

  @override
  visitModule_name(Module_nameContext ctx) {
    // TODO: implement visitModule_name
    
  }

  @override
  visitName(NameContext ctx) {
    // TODO: implement visitName
    
  }

  @override
  visitNew_table_name(New_table_nameContext ctx) {
    // TODO: implement visitNew_table_name
    
  }

  @override
  visitOf_OF_fset(Of_OF_fsetContext ctx) {
    // TODO: implement visitOf_OF_fset
    
  }

  @override
  visitOrder_by_expr(Order_by_exprContext ctx) {
    // TODO: implement visitOrder_by_expr
    
  }

  @override
  visitOrder_by_expr_asc_desc(Order_by_expr_asc_descContext ctx) {
    // TODO: implement visitOrder_by_expr_asc_desc
    
  }

  @override
  visitOrder_by_stmt(Order_by_stmtContext ctx) {
    // TODO: implement visitOrder_by_stmt
    
  }

  @override
  visitOrdering_term(Ordering_termContext ctx) {
    // TODO: implement visitOrdering_term
    
  }

  @override
  visitOver_clause(Over_clauseContext ctx) {
    // TODO: implement visitOver_clause
    
  }

  @override
  visitParse(ParseContext ctx) {
    // TODO: implement visitParse
    
  }

  @override
  visitPartition_by(Partition_byContext ctx) {
    // TODO: implement visitPartition_by
    
  }

  @override
  visitPragma_name(Pragma_nameContext ctx) {
    // TODO: implement visitPragma_name
    
  }

  @override
  visitPragma_stmt(Pragma_stmtContext ctx) {
    // TODO: implement visitPragma_stmt
    
  }

  @override
  visitPragma_value(Pragma_valueContext ctx) {
    // TODO: implement visitPragma_value
    
  }

  @override
  visitQualified_table_name(Qualified_table_nameContext ctx) {
    // TODO: implement visitQualified_table_name
    
  }

  @override
  visitRaise_function(Raise_functionContext ctx) {
    // TODO: implement visitRaise_function
    
  }

  @override
  visitRecursive__select(Recursive__selectContext ctx) {
    // TODO: implement visitRecursive__select
    
  }

  @override
  visitRecursive_cte(Recursive_cteContext ctx) {
    // TODO: implement visitRecursive_cte
    
  }

  @override
  visitReindex_stmt(Reindex_stmtContext ctx) {
    // TODO: implement visitReindex_stmt
    
  }

  @override
  visitRelease_stmt(Release_stmtContext ctx) {
    // TODO: implement visitRelease_stmt
    
  }

  @override
  visitResult_column(Result_columnContext ctx) {
    // TODO: implement visitResult_column
    
  }

  @override
  visitRollback_stmt(Rollback_stmtContext ctx) {
    // TODO: implement visitRollback_stmt
    
  }

  @override
  visitSavepoint_name(Savepoint_nameContext ctx) {
    // TODO: implement visitSavepoint_name
    
  }

  @override
  visitSavepoint_stmt(Savepoint_stmtContext ctx) {
    // TODO: implement visitSavepoint_stmt
    
  }

  @override
  visitSchema_name(Schema_nameContext ctx) {
    // TODO: implement visitSchema_name
    
  }

  @override
  visitSelect_core(Select_coreContext ctx) {
    // TODO: implement visitSelect_core
    
  }

  @override
  visitSelect_stmt(Select_stmtContext ctx) {
    // TODO: implement visitSelect_stmt
    
  }

  @override
  visitSigned_number(Signed_numberContext ctx) {
    // TODO: implement visitSigned_number
    
  }

  @override
  visitSimple_func(Simple_funcContext ctx) {
    // TODO: implement visitSimple_func
    
  }

  @override
  visitSimple_function_invocation(Simple_function_invocationContext ctx) {
    // TODO: implement visitSimple_function_invocation
    
  }

  @override
  visitSimple_select_stmt(Simple_select_stmtContext ctx) {
    // TODO: implement visitSimple_select_stmt
    
  }

  @override
  visitSql_stmt(Sql_stmtContext ctx) {
    // TODO: implement visitSql_stmt
    
  }

  @override
  visitSql_stmt_list(Sql_stmt_listContext ctx) {
    // TODO: implement visitSql_stmt_list
    
  }

  @override
  visitTable_alias(Table_aliasContext ctx) {
    // TODO: implement visitTable_alias
    
  }

  @override
  visitTable_constraint(Table_constraintContext ctx) {
    // TODO: implement visitTable_constraint
    
  }

  @override
  visitTable_function_name(Table_function_nameContext ctx) {
    // TODO: implement visitTable_function_name
    
  }

  @override
  visitTable_name(Table_nameContext ctx) {
    // TODO: implement visitTable_name
    
  }

  @override
  visitTable_or_index_name(Table_or_index_nameContext ctx) {
    // TODO: implement visitTable_or_index_name
    
  }

  @override
  visitTable_or_subquery(Table_or_subqueryContext ctx) {
    // TODO: implement visitTable_or_subquery
    
  }

  @override
  visitTransaction_name(Transaction_nameContext ctx) {
    // TODO: implement visitTransaction_name
    
  }

  @override
  visitTrigger_name(Trigger_nameContext ctx) {
    // TODO: implement visitTrigger_name
    
  }

  @override
  visitType_name(Type_nameContext ctx) {
    // TODO: implement visitType_name
    
  }

  @override
  visitUnary_operator(Unary_operatorContext ctx) {
    // TODO: implement visitUnary_operator
    
  }

  @override
  visitUpdate_stmt(Update_stmtContext ctx) {
    // TODO: implement visitUpdate_stmt
    
  }

  @override
  visitUpdate_stmt_limited(Update_stmt_limitedContext ctx) {
    // TODO: implement visitUpdate_stmt_limited
    
  }

  @override
  visitUpsert_clause(Upsert_clauseContext ctx) {
    // TODO: implement visitUpsert_clause
    
  }

  @override
  visitVacuum_stmt(Vacuum_stmtContext ctx) {
    // TODO: implement visitVacuum_stmt
    
  }

  @override
  visitView_name(View_nameContext ctx) {
    // TODO: implement visitView_name
    
  }

  @override
  visitWindow_defn(Window_defnContext ctx) {
    // TODO: implement visitWindow_defn
    
  }

  @override
  visitWindow_function(Window_functionContext ctx) {
    // TODO: implement visitWindow_function
    
  }

  @override
  visitWindow_function_invocation(Window_function_invocationContext ctx) {
    // TODO: implement visitWindow_function_invocation
    
  }

  @override
  visitWindow_name(Window_nameContext ctx) {
    // TODO: implement visitWindow_name
    
  }

  @override
  visitWith_clause(With_clauseContext ctx) {
    // TODO: implement visitWith_clause
    print("visitWith_clause: ${ctx}");
  }

}