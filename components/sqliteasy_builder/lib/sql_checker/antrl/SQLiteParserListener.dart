// Generated from G:/Flutter/sqliteasy/components/sqliteasy_builder/lib/antrl\SQLiteParser.g4 by ANTLR 4.9.1
// ignore_for_file: unused_import, unused_local_variable, prefer_single_quotes
import 'package:antlr4/antlr4.dart';

import 'SQLiteParser.dart';

/// This abstract class defines a complete listener for a parse tree produced by
/// [SQLiteParser].
abstract class SQLiteParserListener extends ParseTreeListener {
  /// Enter a parse tree produced by [SQLiteParser.parse].
  /// [ctx] the parse tree
  void enterParse(ParseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.parse].
  /// [ctx] the parse tree
  void exitParse(ParseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.sql_stmt_list].
  /// [ctx] the parse tree
  void enterSql_stmt_list(Sql_stmt_listContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.sql_stmt_list].
  /// [ctx] the parse tree
  void exitSql_stmt_list(Sql_stmt_listContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.sql_stmt].
  /// [ctx] the parse tree
  void enterSql_stmt(Sql_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.sql_stmt].
  /// [ctx] the parse tree
  void exitSql_stmt(Sql_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.alter_table_stmt].
  /// [ctx] the parse tree
  void enterAlter_table_stmt(Alter_table_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.alter_table_stmt].
  /// [ctx] the parse tree
  void exitAlter_table_stmt(Alter_table_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.analyze_stmt].
  /// [ctx] the parse tree
  void enterAnalyze_stmt(Analyze_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.analyze_stmt].
  /// [ctx] the parse tree
  void exitAnalyze_stmt(Analyze_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.attach_stmt].
  /// [ctx] the parse tree
  void enterAttach_stmt(Attach_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.attach_stmt].
  /// [ctx] the parse tree
  void exitAttach_stmt(Attach_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.begin_stmt].
  /// [ctx] the parse tree
  void enterBegin_stmt(Begin_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.begin_stmt].
  /// [ctx] the parse tree
  void exitBegin_stmt(Begin_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.commit_stmt].
  /// [ctx] the parse tree
  void enterCommit_stmt(Commit_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.commit_stmt].
  /// [ctx] the parse tree
  void exitCommit_stmt(Commit_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.rollback_stmt].
  /// [ctx] the parse tree
  void enterRollback_stmt(Rollback_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.rollback_stmt].
  /// [ctx] the parse tree
  void exitRollback_stmt(Rollback_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.savepoint_stmt].
  /// [ctx] the parse tree
  void enterSavepoint_stmt(Savepoint_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.savepoint_stmt].
  /// [ctx] the parse tree
  void exitSavepoint_stmt(Savepoint_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.release_stmt].
  /// [ctx] the parse tree
  void enterRelease_stmt(Release_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.release_stmt].
  /// [ctx] the parse tree
  void exitRelease_stmt(Release_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.create_index_stmt].
  /// [ctx] the parse tree
  void enterCreate_index_stmt(Create_index_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.create_index_stmt].
  /// [ctx] the parse tree
  void exitCreate_index_stmt(Create_index_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.indexed_column].
  /// [ctx] the parse tree
  void enterIndexed_column(Indexed_columnContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.indexed_column].
  /// [ctx] the parse tree
  void exitIndexed_column(Indexed_columnContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.create_table_stmt].
  /// [ctx] the parse tree
  void enterCreate_table_stmt(Create_table_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.create_table_stmt].
  /// [ctx] the parse tree
  void exitCreate_table_stmt(Create_table_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.column_def].
  /// [ctx] the parse tree
  void enterColumn_def(Column_defContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.column_def].
  /// [ctx] the parse tree
  void exitColumn_def(Column_defContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.type_name].
  /// [ctx] the parse tree
  void enterType_name(Type_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.type_name].
  /// [ctx] the parse tree
  void exitType_name(Type_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.column_constraint].
  /// [ctx] the parse tree
  void enterColumn_constraint(Column_constraintContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.column_constraint].
  /// [ctx] the parse tree
  void exitColumn_constraint(Column_constraintContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.signed_number].
  /// [ctx] the parse tree
  void enterSigned_number(Signed_numberContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.signed_number].
  /// [ctx] the parse tree
  void exitSigned_number(Signed_numberContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.table_constraint].
  /// [ctx] the parse tree
  void enterTable_constraint(Table_constraintContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.table_constraint].
  /// [ctx] the parse tree
  void exitTable_constraint(Table_constraintContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.foreign_key_clause].
  /// [ctx] the parse tree
  void enterForeign_key_clause(Foreign_key_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.foreign_key_clause].
  /// [ctx] the parse tree
  void exitForeign_key_clause(Foreign_key_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.conflict_clause].
  /// [ctx] the parse tree
  void enterConflict_clause(Conflict_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.conflict_clause].
  /// [ctx] the parse tree
  void exitConflict_clause(Conflict_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.create_trigger_stmt].
  /// [ctx] the parse tree
  void enterCreate_trigger_stmt(Create_trigger_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.create_trigger_stmt].
  /// [ctx] the parse tree
  void exitCreate_trigger_stmt(Create_trigger_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.create_view_stmt].
  /// [ctx] the parse tree
  void enterCreate_view_stmt(Create_view_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.create_view_stmt].
  /// [ctx] the parse tree
  void exitCreate_view_stmt(Create_view_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.create_virtual_table_stmt].
  /// [ctx] the parse tree
  void enterCreate_virtual_table_stmt(Create_virtual_table_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.create_virtual_table_stmt].
  /// [ctx] the parse tree
  void exitCreate_virtual_table_stmt(Create_virtual_table_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.with_clause].
  /// [ctx] the parse tree
  void enterWith_clause(With_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.with_clause].
  /// [ctx] the parse tree
  void exitWith_clause(With_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.cte_table_name].
  /// [ctx] the parse tree
  void enterCte_table_name(Cte_table_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.cte_table_name].
  /// [ctx] the parse tree
  void exitCte_table_name(Cte_table_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.recursive_cte].
  /// [ctx] the parse tree
  void enterRecursive_cte(Recursive_cteContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.recursive_cte].
  /// [ctx] the parse tree
  void exitRecursive_cte(Recursive_cteContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.common_table_expression].
  /// [ctx] the parse tree
  void enterCommon_table_expression(Common_table_expressionContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.common_table_expression].
  /// [ctx] the parse tree
  void exitCommon_table_expression(Common_table_expressionContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.delete_stmt].
  /// [ctx] the parse tree
  void enterDelete_stmt(Delete_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.delete_stmt].
  /// [ctx] the parse tree
  void exitDelete_stmt(Delete_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.delete_stmt_limited].
  /// [ctx] the parse tree
  void enterDelete_stmt_limited(Delete_stmt_limitedContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.delete_stmt_limited].
  /// [ctx] the parse tree
  void exitDelete_stmt_limited(Delete_stmt_limitedContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.detach_stmt].
  /// [ctx] the parse tree
  void enterDetach_stmt(Detach_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.detach_stmt].
  /// [ctx] the parse tree
  void exitDetach_stmt(Detach_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.drop_stmt].
  /// [ctx] the parse tree
  void enterDrop_stmt(Drop_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.drop_stmt].
  /// [ctx] the parse tree
  void exitDrop_stmt(Drop_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.expr].
  /// [ctx] the parse tree
  void enterExpr(ExprContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.expr].
  /// [ctx] the parse tree
  void exitExpr(ExprContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.raise_function].
  /// [ctx] the parse tree
  void enterRaise_function(Raise_functionContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.raise_function].
  /// [ctx] the parse tree
  void exitRaise_function(Raise_functionContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.literal_value].
  /// [ctx] the parse tree
  void enterLiteral_value(Literal_valueContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.literal_value].
  /// [ctx] the parse tree
  void exitLiteral_value(Literal_valueContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.insert_stmt].
  /// [ctx] the parse tree
  void enterInsert_stmt(Insert_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.insert_stmt].
  /// [ctx] the parse tree
  void exitInsert_stmt(Insert_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.upsert_clause].
  /// [ctx] the parse tree
  void enterUpsert_clause(Upsert_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.upsert_clause].
  /// [ctx] the parse tree
  void exitUpsert_clause(Upsert_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.pragma_stmt].
  /// [ctx] the parse tree
  void enterPragma_stmt(Pragma_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.pragma_stmt].
  /// [ctx] the parse tree
  void exitPragma_stmt(Pragma_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.pragma_value].
  /// [ctx] the parse tree
  void enterPragma_value(Pragma_valueContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.pragma_value].
  /// [ctx] the parse tree
  void exitPragma_value(Pragma_valueContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.reindex_stmt].
  /// [ctx] the parse tree
  void enterReindex_stmt(Reindex_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.reindex_stmt].
  /// [ctx] the parse tree
  void exitReindex_stmt(Reindex_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.select_stmt].
  /// [ctx] the parse tree
  void enterSelect_stmt(Select_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.select_stmt].
  /// [ctx] the parse tree
  void exitSelect_stmt(Select_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.join_clause].
  /// [ctx] the parse tree
  void enterJoin_clause(Join_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.join_clause].
  /// [ctx] the parse tree
  void exitJoin_clause(Join_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.select_core].
  /// [ctx] the parse tree
  void enterSelect_core(Select_coreContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.select_core].
  /// [ctx] the parse tree
  void exitSelect_core(Select_coreContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.factored_select_stmt].
  /// [ctx] the parse tree
  void enterFactored_select_stmt(Factored_select_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.factored_select_stmt].
  /// [ctx] the parse tree
  void exitFactored_select_stmt(Factored_select_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.simple_select_stmt].
  /// [ctx] the parse tree
  void enterSimple_select_stmt(Simple_select_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.simple_select_stmt].
  /// [ctx] the parse tree
  void exitSimple_select_stmt(Simple_select_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.compound_select_stmt].
  /// [ctx] the parse tree
  void enterCompound_select_stmt(Compound_select_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.compound_select_stmt].
  /// [ctx] the parse tree
  void exitCompound_select_stmt(Compound_select_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.table_or_subquery].
  /// [ctx] the parse tree
  void enterTable_or_subquery(Table_or_subqueryContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.table_or_subquery].
  /// [ctx] the parse tree
  void exitTable_or_subquery(Table_or_subqueryContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.result_column].
  /// [ctx] the parse tree
  void enterResult_column(Result_columnContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.result_column].
  /// [ctx] the parse tree
  void exitResult_column(Result_columnContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.join_operator].
  /// [ctx] the parse tree
  void enterJoin_operator(Join_operatorContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.join_operator].
  /// [ctx] the parse tree
  void exitJoin_operator(Join_operatorContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.join_constraint].
  /// [ctx] the parse tree
  void enterJoin_constraint(Join_constraintContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.join_constraint].
  /// [ctx] the parse tree
  void exitJoin_constraint(Join_constraintContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.compound_operator].
  /// [ctx] the parse tree
  void enterCompound_operator(Compound_operatorContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.compound_operator].
  /// [ctx] the parse tree
  void exitCompound_operator(Compound_operatorContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.update_stmt].
  /// [ctx] the parse tree
  void enterUpdate_stmt(Update_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.update_stmt].
  /// [ctx] the parse tree
  void exitUpdate_stmt(Update_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.column_name_list].
  /// [ctx] the parse tree
  void enterColumn_name_list(Column_name_listContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.column_name_list].
  /// [ctx] the parse tree
  void exitColumn_name_list(Column_name_listContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.update_stmt_limited].
  /// [ctx] the parse tree
  void enterUpdate_stmt_limited(Update_stmt_limitedContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.update_stmt_limited].
  /// [ctx] the parse tree
  void exitUpdate_stmt_limited(Update_stmt_limitedContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.qualified_table_name].
  /// [ctx] the parse tree
  void enterQualified_table_name(Qualified_table_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.qualified_table_name].
  /// [ctx] the parse tree
  void exitQualified_table_name(Qualified_table_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.vacuum_stmt].
  /// [ctx] the parse tree
  void enterVacuum_stmt(Vacuum_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.vacuum_stmt].
  /// [ctx] the parse tree
  void exitVacuum_stmt(Vacuum_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.filter_clause].
  /// [ctx] the parse tree
  void enterFilter_clause(Filter_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.filter_clause].
  /// [ctx] the parse tree
  void exitFilter_clause(Filter_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.window_defn].
  /// [ctx] the parse tree
  void enterWindow_defn(Window_defnContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.window_defn].
  /// [ctx] the parse tree
  void exitWindow_defn(Window_defnContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.over_clause].
  /// [ctx] the parse tree
  void enterOver_clause(Over_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.over_clause].
  /// [ctx] the parse tree
  void exitOver_clause(Over_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.frame_spec].
  /// [ctx] the parse tree
  void enterFrame_spec(Frame_specContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.frame_spec].
  /// [ctx] the parse tree
  void exitFrame_spec(Frame_specContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.frame_clause].
  /// [ctx] the parse tree
  void enterFrame_clause(Frame_clauseContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.frame_clause].
  /// [ctx] the parse tree
  void exitFrame_clause(Frame_clauseContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.simple_function_invocation].
  /// [ctx] the parse tree
  void enterSimple_function_invocation(Simple_function_invocationContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.simple_function_invocation].
  /// [ctx] the parse tree
  void exitSimple_function_invocation(Simple_function_invocationContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.aggregate_function_invocation].
  /// [ctx] the parse tree
  void enterAggregate_function_invocation(Aggregate_function_invocationContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.aggregate_function_invocation].
  /// [ctx] the parse tree
  void exitAggregate_function_invocation(Aggregate_function_invocationContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.window_function_invocation].
  /// [ctx] the parse tree
  void enterWindow_function_invocation(Window_function_invocationContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.window_function_invocation].
  /// [ctx] the parse tree
  void exitWindow_function_invocation(Window_function_invocationContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.common_table_stmt].
  /// [ctx] the parse tree
  void enterCommon_table_stmt(Common_table_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.common_table_stmt].
  /// [ctx] the parse tree
  void exitCommon_table_stmt(Common_table_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.order_by_stmt].
  /// [ctx] the parse tree
  void enterOrder_by_stmt(Order_by_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.order_by_stmt].
  /// [ctx] the parse tree
  void exitOrder_by_stmt(Order_by_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.limit_stmt].
  /// [ctx] the parse tree
  void enterLimit_stmt(Limit_stmtContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.limit_stmt].
  /// [ctx] the parse tree
  void exitLimit_stmt(Limit_stmtContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.ordering_term].
  /// [ctx] the parse tree
  void enterOrdering_term(Ordering_termContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.ordering_term].
  /// [ctx] the parse tree
  void exitOrdering_term(Ordering_termContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.asc_desc].
  /// [ctx] the parse tree
  void enterAsc_desc(Asc_descContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.asc_desc].
  /// [ctx] the parse tree
  void exitAsc_desc(Asc_descContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.frame_left].
  /// [ctx] the parse tree
  void enterFrame_left(Frame_leftContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.frame_left].
  /// [ctx] the parse tree
  void exitFrame_left(Frame_leftContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.frame_right].
  /// [ctx] the parse tree
  void enterFrame_right(Frame_rightContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.frame_right].
  /// [ctx] the parse tree
  void exitFrame_right(Frame_rightContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.frame_single].
  /// [ctx] the parse tree
  void enterFrame_single(Frame_singleContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.frame_single].
  /// [ctx] the parse tree
  void exitFrame_single(Frame_singleContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.window_function].
  /// [ctx] the parse tree
  void enterWindow_function(Window_functionContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.window_function].
  /// [ctx] the parse tree
  void exitWindow_function(Window_functionContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.of_OF_fset].
  /// [ctx] the parse tree
  void enterOf_OF_fset(Of_OF_fsetContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.of_OF_fset].
  /// [ctx] the parse tree
  void exitOf_OF_fset(Of_OF_fsetContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.default_DEFAULT__value].
  /// [ctx] the parse tree
  void enterDefault_DEFAULT__value(Default_DEFAULT__valueContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.default_DEFAULT__value].
  /// [ctx] the parse tree
  void exitDefault_DEFAULT__value(Default_DEFAULT__valueContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.partition_by].
  /// [ctx] the parse tree
  void enterPartition_by(Partition_byContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.partition_by].
  /// [ctx] the parse tree
  void exitPartition_by(Partition_byContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.order_by_expr].
  /// [ctx] the parse tree
  void enterOrder_by_expr(Order_by_exprContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.order_by_expr].
  /// [ctx] the parse tree
  void exitOrder_by_expr(Order_by_exprContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.order_by_expr_asc_desc].
  /// [ctx] the parse tree
  void enterOrder_by_expr_asc_desc(Order_by_expr_asc_descContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.order_by_expr_asc_desc].
  /// [ctx] the parse tree
  void exitOrder_by_expr_asc_desc(Order_by_expr_asc_descContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.expr_asc_desc].
  /// [ctx] the parse tree
  void enterExpr_asc_desc(Expr_asc_descContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.expr_asc_desc].
  /// [ctx] the parse tree
  void exitExpr_asc_desc(Expr_asc_descContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.initial_select].
  /// [ctx] the parse tree
  void enterInitial_select(Initial_selectContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.initial_select].
  /// [ctx] the parse tree
  void exitInitial_select(Initial_selectContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.recursive__select].
  /// [ctx] the parse tree
  void enterRecursive__select(Recursive__selectContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.recursive__select].
  /// [ctx] the parse tree
  void exitRecursive__select(Recursive__selectContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.unary_operator].
  /// [ctx] the parse tree
  void enterUnary_operator(Unary_operatorContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.unary_operator].
  /// [ctx] the parse tree
  void exitUnary_operator(Unary_operatorContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.error_message].
  /// [ctx] the parse tree
  void enterError_message(Error_messageContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.error_message].
  /// [ctx] the parse tree
  void exitError_message(Error_messageContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.module_argument].
  /// [ctx] the parse tree
  void enterModule_argument(Module_argumentContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.module_argument].
  /// [ctx] the parse tree
  void exitModule_argument(Module_argumentContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.column_alias].
  /// [ctx] the parse tree
  void enterColumn_alias(Column_aliasContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.column_alias].
  /// [ctx] the parse tree
  void exitColumn_alias(Column_aliasContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.keyword].
  /// [ctx] the parse tree
  void enterKeyword(KeywordContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.keyword].
  /// [ctx] the parse tree
  void exitKeyword(KeywordContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.name].
  /// [ctx] the parse tree
  void enterName(NameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.name].
  /// [ctx] the parse tree
  void exitName(NameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.function_name].
  /// [ctx] the parse tree
  void enterFunction_name(Function_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.function_name].
  /// [ctx] the parse tree
  void exitFunction_name(Function_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.schema_name].
  /// [ctx] the parse tree
  void enterSchema_name(Schema_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.schema_name].
  /// [ctx] the parse tree
  void exitSchema_name(Schema_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.table_name].
  /// [ctx] the parse tree
  void enterTable_name(Table_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.table_name].
  /// [ctx] the parse tree
  void exitTable_name(Table_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.table_or_index_name].
  /// [ctx] the parse tree
  void enterTable_or_index_name(Table_or_index_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.table_or_index_name].
  /// [ctx] the parse tree
  void exitTable_or_index_name(Table_or_index_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.new_table_name].
  /// [ctx] the parse tree
  void enterNew_table_name(New_table_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.new_table_name].
  /// [ctx] the parse tree
  void exitNew_table_name(New_table_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.column_name].
  /// [ctx] the parse tree
  void enterColumn_name(Column_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.column_name].
  /// [ctx] the parse tree
  void exitColumn_name(Column_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.collation_name].
  /// [ctx] the parse tree
  void enterCollation_name(Collation_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.collation_name].
  /// [ctx] the parse tree
  void exitCollation_name(Collation_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.foreign_table].
  /// [ctx] the parse tree
  void enterForeign_table(Foreign_tableContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.foreign_table].
  /// [ctx] the parse tree
  void exitForeign_table(Foreign_tableContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.index_name].
  /// [ctx] the parse tree
  void enterIndex_name(Index_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.index_name].
  /// [ctx] the parse tree
  void exitIndex_name(Index_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.trigger_name].
  /// [ctx] the parse tree
  void enterTrigger_name(Trigger_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.trigger_name].
  /// [ctx] the parse tree
  void exitTrigger_name(Trigger_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.view_name].
  /// [ctx] the parse tree
  void enterView_name(View_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.view_name].
  /// [ctx] the parse tree
  void exitView_name(View_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.module_name].
  /// [ctx] the parse tree
  void enterModule_name(Module_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.module_name].
  /// [ctx] the parse tree
  void exitModule_name(Module_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.pragma_name].
  /// [ctx] the parse tree
  void enterPragma_name(Pragma_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.pragma_name].
  /// [ctx] the parse tree
  void exitPragma_name(Pragma_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.savepoint_name].
  /// [ctx] the parse tree
  void enterSavepoint_name(Savepoint_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.savepoint_name].
  /// [ctx] the parse tree
  void exitSavepoint_name(Savepoint_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.table_alias].
  /// [ctx] the parse tree
  void enterTable_alias(Table_aliasContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.table_alias].
  /// [ctx] the parse tree
  void exitTable_alias(Table_aliasContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.transaction_name].
  /// [ctx] the parse tree
  void enterTransaction_name(Transaction_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.transaction_name].
  /// [ctx] the parse tree
  void exitTransaction_name(Transaction_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.window_name].
  /// [ctx] the parse tree
  void enterWindow_name(Window_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.window_name].
  /// [ctx] the parse tree
  void exitWindow_name(Window_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.alias].
  /// [ctx] the parse tree
  void enterAlias(AliasContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.alias].
  /// [ctx] the parse tree
  void exitAlias(AliasContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.filename].
  /// [ctx] the parse tree
  void enterFilename(FilenameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.filename].
  /// [ctx] the parse tree
  void exitFilename(FilenameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.base_window_name].
  /// [ctx] the parse tree
  void enterBase_window_name(Base_window_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.base_window_name].
  /// [ctx] the parse tree
  void exitBase_window_name(Base_window_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.simple_func].
  /// [ctx] the parse tree
  void enterSimple_func(Simple_funcContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.simple_func].
  /// [ctx] the parse tree
  void exitSimple_func(Simple_funcContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.aggregate_func].
  /// [ctx] the parse tree
  void enterAggregate_func(Aggregate_funcContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.aggregate_func].
  /// [ctx] the parse tree
  void exitAggregate_func(Aggregate_funcContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.table_function_name].
  /// [ctx] the parse tree
  void enterTable_function_name(Table_function_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.table_function_name].
  /// [ctx] the parse tree
  void exitTable_function_name(Table_function_nameContext ctx);

  /// Enter a parse tree produced by [SQLiteParser.any_name].
  /// [ctx] the parse tree
  void enterAny_name(Any_nameContext ctx);
  /// Exit a parse tree produced by [SQLiteParser.any_name].
  /// [ctx] the parse tree
  void exitAny_name(Any_nameContext ctx);
}