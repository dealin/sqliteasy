// Generated from G:/Flutter/sqliteasy/components/sqliteasy_builder/lib/antrl\SQLiteParser.g4 by ANTLR 4.9.1
// ignore_for_file: unused_import, unused_local_variable, prefer_single_quotes
import 'package:antlr4/antlr4.dart';

import 'SQLiteParser.dart';
import 'SQLiteParserListener.dart';


/// This class provides an empty implementation of [SQLiteParserListener],
/// which can be extended to create a listener which only needs to handle
/// a subset of the available methods.
class SQLiteParserBaseListener implements SQLiteParserListener {
  /// The default implementation does nothing.
  @override
  void enterParse(ParseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitParse(ParseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSql_stmt_list(Sql_stmt_listContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSql_stmt_list(Sql_stmt_listContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSql_stmt(Sql_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSql_stmt(Sql_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAlter_table_stmt(Alter_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAlter_table_stmt(Alter_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAnalyze_stmt(Analyze_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAnalyze_stmt(Analyze_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAttach_stmt(Attach_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAttach_stmt(Attach_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterBegin_stmt(Begin_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitBegin_stmt(Begin_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCommit_stmt(Commit_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCommit_stmt(Commit_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterRollback_stmt(Rollback_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitRollback_stmt(Rollback_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSavepoint_stmt(Savepoint_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSavepoint_stmt(Savepoint_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterRelease_stmt(Release_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitRelease_stmt(Release_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCreate_index_stmt(Create_index_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCreate_index_stmt(Create_index_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterIndexed_column(Indexed_columnContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitIndexed_column(Indexed_columnContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCreate_table_stmt(Create_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCreate_table_stmt(Create_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterColumn_def(Column_defContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitColumn_def(Column_defContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterType_name(Type_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitType_name(Type_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterColumn_constraint(Column_constraintContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitColumn_constraint(Column_constraintContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSigned_number(Signed_numberContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSigned_number(Signed_numberContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTable_constraint(Table_constraintContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTable_constraint(Table_constraintContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterForeign_key_clause(Foreign_key_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitForeign_key_clause(Foreign_key_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterConflict_clause(Conflict_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitConflict_clause(Conflict_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCreate_trigger_stmt(Create_trigger_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCreate_trigger_stmt(Create_trigger_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCreate_view_stmt(Create_view_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCreate_view_stmt(Create_view_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCreate_virtual_table_stmt(Create_virtual_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCreate_virtual_table_stmt(Create_virtual_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterWith_clause(With_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitWith_clause(With_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCte_table_name(Cte_table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCte_table_name(Cte_table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterRecursive_cte(Recursive_cteContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitRecursive_cte(Recursive_cteContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCommon_table_expression(Common_table_expressionContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCommon_table_expression(Common_table_expressionContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterDelete_stmt(Delete_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitDelete_stmt(Delete_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterDelete_stmt_limited(Delete_stmt_limitedContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitDelete_stmt_limited(Delete_stmt_limitedContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterDetach_stmt(Detach_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitDetach_stmt(Detach_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterDrop_stmt(Drop_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitDrop_stmt(Drop_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterExpr(ExprContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitExpr(ExprContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterRaise_function(Raise_functionContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitRaise_function(Raise_functionContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterLiteral_value(Literal_valueContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitLiteral_value(Literal_valueContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterInsert_stmt(Insert_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitInsert_stmt(Insert_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterUpsert_clause(Upsert_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitUpsert_clause(Upsert_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterPragma_stmt(Pragma_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitPragma_stmt(Pragma_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterPragma_value(Pragma_valueContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitPragma_value(Pragma_valueContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterReindex_stmt(Reindex_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitReindex_stmt(Reindex_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSelect_stmt(Select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSelect_stmt(Select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterJoin_clause(Join_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitJoin_clause(Join_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSelect_core(Select_coreContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSelect_core(Select_coreContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFactored_select_stmt(Factored_select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFactored_select_stmt(Factored_select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSimple_select_stmt(Simple_select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSimple_select_stmt(Simple_select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCompound_select_stmt(Compound_select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCompound_select_stmt(Compound_select_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTable_or_subquery(Table_or_subqueryContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTable_or_subquery(Table_or_subqueryContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterResult_column(Result_columnContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitResult_column(Result_columnContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterJoin_operator(Join_operatorContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitJoin_operator(Join_operatorContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterJoin_constraint(Join_constraintContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitJoin_constraint(Join_constraintContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCompound_operator(Compound_operatorContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCompound_operator(Compound_operatorContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterUpdate_stmt(Update_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitUpdate_stmt(Update_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterColumn_name_list(Column_name_listContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitColumn_name_list(Column_name_listContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterUpdate_stmt_limited(Update_stmt_limitedContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitUpdate_stmt_limited(Update_stmt_limitedContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterQualified_table_name(Qualified_table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitQualified_table_name(Qualified_table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterVacuum_stmt(Vacuum_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitVacuum_stmt(Vacuum_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFilter_clause(Filter_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFilter_clause(Filter_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterWindow_defn(Window_defnContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitWindow_defn(Window_defnContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterOver_clause(Over_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitOver_clause(Over_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFrame_spec(Frame_specContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFrame_spec(Frame_specContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFrame_clause(Frame_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFrame_clause(Frame_clauseContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSimple_function_invocation(Simple_function_invocationContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSimple_function_invocation(Simple_function_invocationContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAggregate_function_invocation(Aggregate_function_invocationContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAggregate_function_invocation(Aggregate_function_invocationContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterWindow_function_invocation(Window_function_invocationContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitWindow_function_invocation(Window_function_invocationContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCommon_table_stmt(Common_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCommon_table_stmt(Common_table_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterOrder_by_stmt(Order_by_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitOrder_by_stmt(Order_by_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterLimit_stmt(Limit_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitLimit_stmt(Limit_stmtContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterOrdering_term(Ordering_termContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitOrdering_term(Ordering_termContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAsc_desc(Asc_descContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAsc_desc(Asc_descContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFrame_left(Frame_leftContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFrame_left(Frame_leftContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFrame_right(Frame_rightContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFrame_right(Frame_rightContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFrame_single(Frame_singleContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFrame_single(Frame_singleContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterWindow_function(Window_functionContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitWindow_function(Window_functionContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterOf_OF_fset(Of_OF_fsetContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitOf_OF_fset(Of_OF_fsetContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterDefault_DEFAULT__value(Default_DEFAULT__valueContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitDefault_DEFAULT__value(Default_DEFAULT__valueContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterPartition_by(Partition_byContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitPartition_by(Partition_byContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterOrder_by_expr(Order_by_exprContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitOrder_by_expr(Order_by_exprContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterOrder_by_expr_asc_desc(Order_by_expr_asc_descContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitOrder_by_expr_asc_desc(Order_by_expr_asc_descContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterExpr_asc_desc(Expr_asc_descContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitExpr_asc_desc(Expr_asc_descContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterInitial_select(Initial_selectContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitInitial_select(Initial_selectContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterRecursive__select(Recursive__selectContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitRecursive__select(Recursive__selectContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterUnary_operator(Unary_operatorContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitUnary_operator(Unary_operatorContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterError_message(Error_messageContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitError_message(Error_messageContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterModule_argument(Module_argumentContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitModule_argument(Module_argumentContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterColumn_alias(Column_aliasContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitColumn_alias(Column_aliasContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterKeyword(KeywordContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitKeyword(KeywordContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterName(NameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitName(NameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFunction_name(Function_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFunction_name(Function_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSchema_name(Schema_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSchema_name(Schema_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTable_name(Table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTable_name(Table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTable_or_index_name(Table_or_index_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTable_or_index_name(Table_or_index_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterNew_table_name(New_table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitNew_table_name(New_table_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterColumn_name(Column_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitColumn_name(Column_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterCollation_name(Collation_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitCollation_name(Collation_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterForeign_table(Foreign_tableContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitForeign_table(Foreign_tableContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterIndex_name(Index_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitIndex_name(Index_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTrigger_name(Trigger_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTrigger_name(Trigger_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterView_name(View_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitView_name(View_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterModule_name(Module_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitModule_name(Module_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterPragma_name(Pragma_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitPragma_name(Pragma_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSavepoint_name(Savepoint_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSavepoint_name(Savepoint_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTable_alias(Table_aliasContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTable_alias(Table_aliasContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTransaction_name(Transaction_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTransaction_name(Transaction_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterWindow_name(Window_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitWindow_name(Window_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAlias(AliasContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAlias(AliasContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterFilename(FilenameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitFilename(FilenameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterBase_window_name(Base_window_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitBase_window_name(Base_window_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterSimple_func(Simple_funcContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitSimple_func(Simple_funcContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAggregate_func(Aggregate_funcContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAggregate_func(Aggregate_funcContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterTable_function_name(Table_function_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitTable_function_name(Table_function_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterAny_name(Any_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitAny_name(Any_nameContext ctx) {}

  /// The default implementation does nothing.
  @override
  void enterEveryRule(ParserRuleContext ctx) {}

  /// The default implementation does nothing.
  @override
  void exitEveryRule(ParserRuleContext ctx) {}

  /// The default implementation does nothing.
  @override
  void visitTerminal(TerminalNode node) {}

  /// The default implementation does nothing.
  @override
  void visitErrorNode(ErrorNode node) {}
}
