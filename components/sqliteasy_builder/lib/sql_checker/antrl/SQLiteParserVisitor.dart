// Generated from G:/Flutter/sqliteasy/components/sqliteasy_builder/lib/antrl\SQLiteParser.g4 by ANTLR 4.9.1
// ignore_for_file: unused_import, unused_local_variable, prefer_single_quotes
import 'package:antlr4/antlr4.dart';

import 'SQLiteParser.dart';

/// This abstract class defines a complete generic visitor for a parse tree
/// produced by [SQLiteParser].
///
/// [T] is the eturn type of the visit operation. Use `void` for
/// operations with no return type.
abstract class SQLiteParserVisitor<T> extends ParseTreeVisitor<T> {
  /// Visit a parse tree produced by [SQLiteParser.parse].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitParse(ParseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.sql_stmt_list].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSql_stmt_list(Sql_stmt_listContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.sql_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSql_stmt(Sql_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.alter_table_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAlter_table_stmt(Alter_table_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.analyze_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAnalyze_stmt(Analyze_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.attach_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAttach_stmt(Attach_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.begin_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitBegin_stmt(Begin_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.commit_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCommit_stmt(Commit_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.rollback_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitRollback_stmt(Rollback_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.savepoint_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSavepoint_stmt(Savepoint_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.release_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitRelease_stmt(Release_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.create_index_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCreate_index_stmt(Create_index_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.indexed_column].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitIndexed_column(Indexed_columnContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.create_table_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCreate_table_stmt(Create_table_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.column_def].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitColumn_def(Column_defContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.type_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitType_name(Type_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.column_constraint].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitColumn_constraint(Column_constraintContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.signed_number].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSigned_number(Signed_numberContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.table_constraint].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTable_constraint(Table_constraintContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.foreign_key_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitForeign_key_clause(Foreign_key_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.conflict_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitConflict_clause(Conflict_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.create_trigger_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCreate_trigger_stmt(Create_trigger_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.create_view_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCreate_view_stmt(Create_view_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.create_virtual_table_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCreate_virtual_table_stmt(Create_virtual_table_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.with_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitWith_clause(With_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.cte_table_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCte_table_name(Cte_table_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.recursive_cte].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitRecursive_cte(Recursive_cteContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.common_table_expression].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCommon_table_expression(Common_table_expressionContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.delete_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitDelete_stmt(Delete_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.delete_stmt_limited].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitDelete_stmt_limited(Delete_stmt_limitedContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.detach_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitDetach_stmt(Detach_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.drop_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitDrop_stmt(Drop_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.expr].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitExpr(ExprContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.raise_function].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitRaise_function(Raise_functionContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.literal_value].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitLiteral_value(Literal_valueContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.insert_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitInsert_stmt(Insert_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.upsert_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitUpsert_clause(Upsert_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.pragma_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitPragma_stmt(Pragma_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.pragma_value].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitPragma_value(Pragma_valueContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.reindex_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitReindex_stmt(Reindex_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.select_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSelect_stmt(Select_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.join_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitJoin_clause(Join_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.select_core].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSelect_core(Select_coreContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.factored_select_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFactored_select_stmt(Factored_select_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.simple_select_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSimple_select_stmt(Simple_select_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.compound_select_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCompound_select_stmt(Compound_select_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.table_or_subquery].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTable_or_subquery(Table_or_subqueryContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.result_column].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitResult_column(Result_columnContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.join_operator].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitJoin_operator(Join_operatorContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.join_constraint].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitJoin_constraint(Join_constraintContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.compound_operator].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCompound_operator(Compound_operatorContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.update_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitUpdate_stmt(Update_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.column_name_list].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitColumn_name_list(Column_name_listContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.update_stmt_limited].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitUpdate_stmt_limited(Update_stmt_limitedContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.qualified_table_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitQualified_table_name(Qualified_table_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.vacuum_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitVacuum_stmt(Vacuum_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.filter_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFilter_clause(Filter_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.window_defn].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitWindow_defn(Window_defnContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.over_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitOver_clause(Over_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.frame_spec].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFrame_spec(Frame_specContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.frame_clause].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFrame_clause(Frame_clauseContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.simple_function_invocation].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSimple_function_invocation(Simple_function_invocationContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.aggregate_function_invocation].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAggregate_function_invocation(Aggregate_function_invocationContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.window_function_invocation].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitWindow_function_invocation(Window_function_invocationContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.common_table_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCommon_table_stmt(Common_table_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.order_by_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitOrder_by_stmt(Order_by_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.limit_stmt].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitLimit_stmt(Limit_stmtContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.ordering_term].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitOrdering_term(Ordering_termContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.asc_desc].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAsc_desc(Asc_descContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.frame_left].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFrame_left(Frame_leftContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.frame_right].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFrame_right(Frame_rightContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.frame_single].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFrame_single(Frame_singleContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.window_function].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitWindow_function(Window_functionContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.of_OF_fset].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitOf_OF_fset(Of_OF_fsetContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.default_DEFAULT__value].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitDefault_DEFAULT__value(Default_DEFAULT__valueContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.partition_by].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitPartition_by(Partition_byContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.order_by_expr].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitOrder_by_expr(Order_by_exprContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.order_by_expr_asc_desc].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitOrder_by_expr_asc_desc(Order_by_expr_asc_descContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.expr_asc_desc].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitExpr_asc_desc(Expr_asc_descContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.initial_select].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitInitial_select(Initial_selectContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.recursive__select].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitRecursive__select(Recursive__selectContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.unary_operator].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitUnary_operator(Unary_operatorContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.error_message].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitError_message(Error_messageContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.module_argument].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitModule_argument(Module_argumentContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.column_alias].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitColumn_alias(Column_aliasContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.keyword].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitKeyword(KeywordContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitName(NameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.function_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFunction_name(Function_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.schema_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSchema_name(Schema_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.table_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTable_name(Table_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.table_or_index_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTable_or_index_name(Table_or_index_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.new_table_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitNew_table_name(New_table_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.column_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitColumn_name(Column_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.collation_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitCollation_name(Collation_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.foreign_table].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitForeign_table(Foreign_tableContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.index_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitIndex_name(Index_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.trigger_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTrigger_name(Trigger_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.view_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitView_name(View_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.module_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitModule_name(Module_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.pragma_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitPragma_name(Pragma_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.savepoint_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSavepoint_name(Savepoint_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.table_alias].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTable_alias(Table_aliasContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.transaction_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTransaction_name(Transaction_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.window_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitWindow_name(Window_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.alias].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAlias(AliasContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.filename].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitFilename(FilenameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.base_window_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitBase_window_name(Base_window_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.simple_func].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitSimple_func(Simple_funcContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.aggregate_func].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAggregate_func(Aggregate_funcContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.table_function_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitTable_function_name(Table_function_nameContext ctx);

  /// Visit a parse tree produced by [SQLiteParser.any_name].
  /// [ctx] the parse tree.
  /// Return the visitor result.
  T visitAny_name(Any_nameContext ctx);
}