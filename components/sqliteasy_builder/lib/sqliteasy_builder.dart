library sqliteasy_core;

import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'builders/database_builder.dart';

export 'model/dao.dart';
export 'model/database.dart';
export 'util/database_util.dart';
export 'errors.dart';

Builder databaseBuilder(BuilderOptions options) {
  print("construct builder");
  return LibraryBuilder(DatabaseBuilder(),
      generatedExtension: '.b.dart');
}