import 'dart:async';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'package:sqliteasy_core/annotations/dao.dart';
import 'package:sqliteasy_core/sqliteasy_core.dart';
import '../model/code_fragment.dart';
import '../model/code_fragment.dart';
import '../model/database.dart';
import '../model/code_fragment.dart';

class DaoBuilder extends Generator {
  FutureOr<String> generate(LibraryReader library, BuildStep buildStep) {
    var daoList = library.annotatedWith(TypeChecker.fromRuntime(Dao));
    print("generate Dao impl: ${daoList}");
    List<MapEntry<SqlStatement, CodeFragment>> insertionList = List();
    List<MapEntry<SqlStatement, CodeFragment>> codeFragments = List();
    daoList.forEach((AnnotatedElement element) {
      if (!(element.element is ClassElement)) {
        return;
      }
      var classElement = element.element as ClassElement;
      classElement.methods.forEach((MethodElement methodElement) {
        /*MapEntry<SqlStatement, CodeFragment> result = DaoUtil.generateSqlStatement(methodElement);
        print("generate statement:${result}");
        if (result == null || result.value == null) {
          return;
        }
        codeFragments.add(result);
        if (result.key.methodType == DaoMethodType.Insert) {
          insertionList.add(result);
        }*/
      });
    });
    Set<String> imports = Set();
    for (var value in codeFragments) {
      imports.addAll(value.value.importList);
    }
    return "${imports.map((e) => "import \"$e\";").toList().join()}"+
        "\n${insertionList.map((e) => e.value?.content ?? "").toList().join("\n")}";
  }
}