class CodeFragment {
  final String content;
  final List<String> importList;

  const CodeFragment(this.content, [this.importList]);
}