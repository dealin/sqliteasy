import 'package:analyzer/dart/element/type.dart';
import 'package:sqliteasy_core/enums/conflict_strategy.dart';
import 'package:sqliteasy_core/sqliteasy_core.dart';

class InsertInfo {
  DartType entity;
  ConflictStrategy conflictStrategy;
}

class DeleteInfo {
  DartType entity;

  DeleteInfo(this.entity);
}

class UpdateInfo {
  ConflictStrategy conflictStrategy;
  NullValueStrategy nullValueStrategy;

  UpdateInfo(this.conflictStrategy, this.nullValueStrategy);
}

class QueryInfo {
  String statement;

  QueryInfo(this.statement);
}