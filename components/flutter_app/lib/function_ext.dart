extension FunctionExt<A, B> on Function(A arg1, B arg2) {
  void callExt(A arg1, B arg2) {
    print("before call Ext");
    this.call(arg1, arg2);
    print("after call Ext");
  }
}

extension Function1Ext<A, B> on B Function(A arg) {
  B callExt(A arg) {
    return this.call(arg);
  }

  void beforeInvoke(Function(A arg) callback) {

  }

  void afterInvoke(Function(A arg) callback) {

  }
}