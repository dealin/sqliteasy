import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/bottom_bar_layout.dart';
import 'package:get_it/get_it.dart';
import 'package:utf/utf.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'function_ext.dart';

abstract class ILogger {
  void logD(String tag, String msg);
}

class ViewModel {
  int count;
  void addCount(int addNum) {

  }
}
class Logger implements ILogger {
  @override
  void logD(String tag, String msg) {
    String source = " 🤣 🥲 ☺️ 😊 😇 🙂 🙃";
    final bytes = encodeUtf16le(source);
    final result = decodeUtf16le(bytes);
    print(result);
  }
}
void testFunc(String a, String b) {
  print("执行函数");
}
void main() {
  runApp(MyApp());
  GetIt.instance.registerFactory<ILogger>(() => Logger());
  ILogger result = GetIt.instance.get();
  result.logD("tag", "test");
  if (Platform.isAndroid) {
    // 以下两行 设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
    SystemUiOverlayStyle systemUiOverlayStyle =
    SystemUiOverlayStyle(statusBarColor: Colors.transparent, systemNavigationBarColor: Colors.blue);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }

  void Function(String arg1, String arg2) func1 = (arg1, arg2){
    print("执行函数");
  };
  Type type = func1.runtimeType;
  testFunc.callExt("1", "2");
  print(type);

  final model1 = ViewModel();
  final model2 = ViewModel();
  bool equal = model1.addCount == model2.addCount;
  print(equal);
}

class MyApp extends StatelessWidget {
  String content;
  MyApp() {
    String source = " 🤣 🥲 ☺️ 😊 😇 🙂 🙃";
    final bytes = encodeUtf16le(source);
    content  = decodeUtf16le(bytes);
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      /*builder: (BuildContext, widget) {
        return MaterialApp(home: Draggable(child: Text("测试12"),feedback: Text("ceui"),),);
      },*/
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          color: Colors.white,
          alignment: Alignment.bottomCenter,
          child: Text("content"),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
