enum Modifier {
  Final, Const, Dynamic, Async, AsyncStar
}