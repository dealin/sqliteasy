import 'base.dart';
import 'type.dart';

class ClassSurrogate {
  final _modifiers = List<Modifier>();
  String _name = "";
  String _superClass = "";
  List<TypeSurrogate> _interfaces = List<TypeSurrogate>();
}