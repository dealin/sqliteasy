import 'package:sqliteasy_core/core/sqliteasy_database.dart';

class SQLiteUtil {
  static Future<bool> isTableExisted(SupportSQLiteDatabase db, String tableName) async {
    List result = await db.query("SELECT count(*) as count FROM sqlite_master WHERE type = 'table' AND name='"
        + tableName + "'");
    if (result.isEmpty) {
      return false;
    }
    return result[0]['count'] > 0;
  }

  static Future<bool> hasSqliteasyMasterTable(SupportSQLiteDatabase db) {
    return isTableExisted(db, sqliteasyMasterTableName);
  }
}