/// @author: dealin
/// created at 2020/12/29 23:12
import "../enums/conflict_strategy.dart";
import "../enums/null_value_strategy.dart";

class Dao {
  const Dao();
}

class DaoAction {
  const DaoAction();
}

class Query extends DaoAction {
  final String statement;

  const Query([this.statement = ""]) : super();
}

class Update extends DaoAction {

  /// What to do if a conflict happens.
  /// Use [ConflictStrategy.abort] to roll back the transaction.
  /// Use [ConflictStrategy.replace] to replace the existing rows with new rows.
  /// Use [ConflictStrategy.ignore] to keep existing rows.
  final ConflictStrategy conflictStrategy;

  final NullValueStrategy nullValueStrategy;

  const Update({this.conflictStrategy = ConflictStrategy.abort, this.nullValueStrategy = NullValueStrategy.unset});
}

class Delete extends DaoAction {
  final Type entity;

  const Delete({this.entity});
}

class Insert extends DaoAction {
  final Type entity;

  /// What to do if a conflict happens.
  /// Use [ConflictStrategy.abort] to roll back the transaction.
  /// Use [ConflictStrategy.replace] to replace the existing rows with new rows.
  /// Use [ConflictStrategy.ignore] to keep existing rows.
  final ConflictStrategy conflictStrategy;

  const Insert({this.entity, this.conflictStrategy = ConflictStrategy.abort});
}
