import 'dart:typed_data';

class ValueConverterFactor {
  static void register<T>(ValueConverter<T> converter) {}
}

abstract class ValueConverter<T> {
  const ValueConverter();
  // 将输入数据转换为T
  List<T> convert(input);
}

class BytesValueConverter extends ValueConverter<Uint8List> {
  @override
  List<Uint8List> convert(input) {
    // TODO: implement convert
    throw UnimplementedError();
  }

}