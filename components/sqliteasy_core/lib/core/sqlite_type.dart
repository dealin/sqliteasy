part of 'sqliteasy_database.dart';

enum SQLiteType { text, integer, real, blob }

extension SQLiteTypeExt on SQLiteType {
  String get typeName {
    switch (this) {
      case SQLiteType.text:
        return SQLiteTypes.text;
      case SQLiteType.integer:
        return SQLiteTypes.integer;
      case SQLiteType.real:
        return SQLiteTypes.real;
      case SQLiteType.blob:
        return SQLiteTypes.blob;
    }
  }
}

class SQLiteTypes {
  static const text = "TEXT";
  static const integer = "INTEGER";
  static const real = "REAL";
  static const blob = "BLOB";

  static SQLiteType getTypeFromName(String name) {
    switch(name.toUpperCase()) {
      case text:
        return SQLiteType.text;
      case integer:
        return SQLiteType.integer;
      case real:
        return SQLiteType.real;
      case blob:
        return SQLiteType.blob;
      default:
        throw Exception("unsupported type name ${name}.");
    }
  }
}
