/// @author: dealin
/// created at 2020/12/29 23:30

enum ConflictStrategy {
  abort,
  replace,
  ignore
}