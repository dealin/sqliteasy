enum NullValueStrategy {
  // 未设置默认状态，如果为unset，则跟随Database的设置
  unset,
  // 当entity的值为null时，则忽略该属性值，不进行更新
  ignore,
  // 应用空值
  apply
}